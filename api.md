# API

***

**URL**: *http://api.clubitalia.app*

* [Messages](api.md#markdown-header-messages)

***

## Messages

+ **GET** */messages/:id*
    + **Description**
    > GET a Message by its ID

    + **Example**
    > ```
    > curl -i http://api.clubitalia.app/messages/1
    > ```

    + **Parameters**
    > Parameter | Type | Description
    > ----------|------|------------
    > id        | int  | Message ID

    + **200 Success**
    > Field   | Type   | Description
    > --------|--------|------------
    > id      | int    | Message ID
    > message | string | Text

    + **400 Error**
    > Code | Error         | Description
    > -----|---------------|----------------------------------------------
    > 401  | NoAccessRight | User is not authorized to perform this action
    > 404  | NotFound      | Message was not found
