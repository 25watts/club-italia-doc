# Club Italia Documentación

***

## Apps

![Apps](images/club-italia.png)

***

## API

* **API** es la aplicación encargada de manejar los datos y sus interacciones.
    Es su objetivo recibir parámetros y regresar una respuesta ocultando todo
    el código interno, ante parámetros iguales debe regresar la misma respuesta.

    Las respuestas son en estructura JSON con información importante según el
    código de estatus HTTP:

    * 200 Success.
    * 201 Created.
    * 204 No Content.
    * 401 No Authorized.
    * 404 Not Found.
    * 409 Conflict.

    En caso de retornar un error 40* además debe retornar un JSON con mayor
    información sobre el error:

    * NoAccessRight: User is not authorized to perform this action.
    * NotFound: Resource was not found.
    * Required*: Parameter * is required.
    * Wrong*: Parameter * is malformed.

    Los HTTP Methods que acepta son los siguientes:

    * GET | HEAD para recuperar un recurso.
    * POST para crear un nuevo recurso.
    * PUT | PATCH para modificar un recurso.
    * DELETE para eliminar un recurso.

    Cada llamada debe ser acompañada por un Access Token que **OAuth2**
    verificará si los permisos son suficientes para realizar la acción y, caso
    contrario, retornará un error.
  
* [Messages](api.md#markdown-header-messages)

***

## OAuth2

* **OAuth2** es la aplicación responsable de administrar a los usuarios y sus
    permisos, además es responsable de verificar que el Access Token enviado a
    una aplicación que haya iniciado sesión con OAuth tenga acceso y los scopes
    estén autorizados a ingresar.

    ![OAuth](images/oauth-diagram.png)

    Es su objetivo reconocer y administrar usuarios, brindar la información de
    los mismos y dar seguridad a **API** ya que allí se administra información
    sensible.

***

## Chat

* **Chat** es la aplicación responsable de conectar a los usuarios de Club
    Italia en tiempo real. Su objetivo es recibir mensajes de los usuarios
    online y crear un stream de mensajes que otros usuarios puedan ver al
    instante. Es también su objetivo crear una conversación entre dos usuarios
    y que al recibir un mensaje de uno de los usuarios se emita al otro.

    Se divide en dos partes:
    
    * *Socket*: Responsable de escuchar y emitir mensajes. Debe escuchar los
        eventos que se produzcan en *APP*, transmitir los parámetros del
        evento a **API** y emitir el resultado de esos parámetros de regreso a
        *APP*
    * *APP*: Responsable de emitir y mostrar mensajes. Debe recibir las
        acciones del usuario y transformarlos en eventos enviados a *Socket*,
        debe escuchar los eventos provenientes de *Socket* y mostrar la
        información organizada. Por otro lado debe hacerse cargo de que los
        usuarios que ingresen en la aplicación hayan iniciado sesión en Club
        Italia.

* [Casos de uso](chat.md#markdown-header-casos-de-uso)

***
