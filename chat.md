# CHAT

***

## Casos de uso

* [Caso 1: Usuario debe loguearse.](chat.md#markdown-header-caso-1-usuario-debe-loguearse)
* [Caso 2: Usuario ingresa.](chat.md#markdown-header-caso-2-usuario-ingresa)
* [Caso 3: Usuario ve mensajes.](chat.md#markdown-header-caso-3-usuario-ve-mensajes)
* [Caso 4: Usuario escribe un mensaje.](chat.md#markdown-header-caso-4-usuario-escribe-un-mensaje)
* [Caso 5: Usuario escribe un mensaje privado.](chat.md#markdown-header-caso-5-usuario-escribe-un-mensaje-privado)
* [Caso 6: Usuario abre un mensaje privado.](chat.md#markdown-header-caso-6-usuario-abre-un-mensaje-privado)
* [Caso 7: Usuario elimina un mensaje privado.](chat.md#markdown-header-caso-7-usuario-elimina-un-mensaje-privado)
* [Caso 8: Usuario cierra aplicación.](chat.md#markdown-header-caso-8-usuario-cierra-aplicacion)

***

## Flujo

![Workflow](images/flow.png)

***

### Caso 1: Usuario debe loguearse.

1. Usuario ingresa sin estar logueado.
2. Usuario es redireccionado a login.
3. Usuario se loguea.
4. Usuario es redireccionado a la aplicación.

### Caso 2: Usuario ingresa.

1. Usuario ingresa estando logueado.
2. Usuario es redireccionado a la aplicación.

### Caso 3: Usuario ve mensajes.

1. Usuario ingresa a la aplicación y ve todos los usuarios logueados.
2. Usuario ve el stream de mensajes de usuarios a partir del momento en el que se loguea.

### Caso 4: Usuario escribe un mensaje.

1. Usuario escribe un mensaje.
2. Usuario envía un mensaje al servidor y se agrega al stream de mensajes.

### Caso 5: Usuario escribe un mensaje privado.

1. Usuario 2 envía mensaje privado a Usuario.
2. Usuario recibe mensaje privado.

### Caso 6: Usuario abre un mensaje privado.

1. Usuario abre mensaje privado de Usuario 2.
2. Usuario lee mensaje privado.

### Caso 7: Usuario elimina un mensaje privado.

1. Usuario 2 elimina mensaje privado.
2. Usuario no puede ver mensaje privado eliminado.

### Caso 8: Usuario cierra aplicación.

1. Usuario 2 cierra aplicación.
2. Usuario ve desconexión Usuario 2.
